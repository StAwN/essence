package ResetThosePacks;

//import ResetThosePacks.SystemInfo;

import javafx.application.Application;
//import javafx.beans.value.ChangeListener;
//import javafx.beans.value.ObservableValue;
//import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
//import javafx.scene.layout.StackPane;
//import javafx.scene.shape.Circle;
//import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
//import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

//Pong imports
import java.util.Random;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
//import javafx.application.Application;
//import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
//import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
//import javafx.stage.Stage;
import javafx.util.Duration;

//import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;

import java.sql.Timestamp;
import java.time.Instant;
//import java.sql.*;
//import java.util.List;
//import java.util.Optional;

import com.dieselpoint.norm.Database;

/**
 * JavaFX App
 */
public class App extends Application {

	private TextField text = new TextField();
	private PasswordField pass = new PasswordField();
	private Label lbl = new Label("Not connected");
	private Button btn = new Button ("Connect");
	private Button btn3 = new Button ("Play Pong");
	private Button btn4 = new Button ("Create User");
	
    private Database db = new Database();
    
    //use this to make sql requests
    //private TestMyDB userConnect = new TestMyDB();
    
    //private BorderPane root7 = new BorderPane();
    private BorderPane root = new BorderPane();
    private Scene scene = new Scene(root, 640, 480);
    
    //Pong vars
    private static final int width = 800;
	private static final int height = 600;
	private static final int PLAYER_HEIGHT = 100;
	private static final int PLAYER_WIDTH = 15;
	private static final double BALL_R = 15;
	private int ballYSpeed = 1;
	private int ballXSpeed = 1;
	private double playerOneYPos = height / 2;
	private double playerTwoYPos = height / 2;
	private double ballXPos = width / 2;
	private double ballYPos = height / 2;
	private int scoreP1 = 0;
	private int scoreP2 = 0;
	private boolean gameStarted;
	private int playerOneXPos = 0;
	private double playerTwoXPos = width - PLAYER_WIDTH;
	private Stage stage;
	
	public int idPused; // this will save player id at Connect click, to be used at Play click
	public int maxScore; // this will store user max score to be updated
	public String nameRecall; // same
	
	@Override
    public void start(Stage stage) {
    	//var javaVersion = SystemInfo.javaVersion();
        //var javafxVersion = SystemInfo.javafxVersion();

        //var label = new Label("Hello, JavaFX " + javafxVersion + ", running on Java " + javaVersion + ".");
        //var lbl = new Label("What doink");
        //var font = new Font(20);
        
        //btn.setFont(20);
        //var root = new BorderPane();
        var box = new HBox();
        
        box.getChildren().add(text);
        box.getChildren().add(pass);
        box.getChildren().add(btn);
        box.getChildren().add(btn3);
        box.getChildren().add(btn4);
        //root.setCenter(listView);
        root.setBottom(box);
        box.setPadding(new Insets(60));
        
        //btn.setShape(new Circle(30));
        //btn.setPrefSize(50,50);
        btn.setOnAction(new HandleClick());
        btn3.setOnAction(new HandleClick2());
        btn4.setOnAction(new HandleClick3());
        
        lbl.setPrefSize(640, 50);
        lbl.setTextFill(Color.INDIANRED);
        lbl.setFont(new Font("Verdana", 14));
        lbl.setPadding(new Insets (60));
        root.setTop(lbl);
        
        //Database connection - we need to do this only once
        db.setJdbcUrl("jdbc:mysql://localhost:3306/pong_csc_12?useSSL=false");
        db.setUser("root");
        db.setPassword("");
        
        //var scene = new Scene(root, 640, 480);
        this.stage = stage; //ty Bonbek //otherwise we have a null stage at HandleClick2
        
        stage.setTitle("Connect&Play");
        stage.setScene(scene);
        stage.show();
    }
	
	//Pong's run using canvas
	private void run(GraphicsContext gc) {
		gc.setFill(Color.BLACK);
		gc.fillRect(0, 0, width, height);
		gc.setFill(Color.WHITE);
		gc.setFont(Font.font(25));
		if(gameStarted) {
			ballXPos+=ballXSpeed;
			ballYPos+=ballYSpeed;
			//Setting Bot pos
			if(ballXPos < width - width  / 4) {
				playerTwoYPos = ballYPos - PLAYER_HEIGHT / 2;
			}  else {
				playerTwoYPos =  ballYPos > playerTwoYPos + PLAYER_HEIGHT / 2 ?playerTwoYPos += 1: playerTwoYPos - 1;
			}
			gc.fillOval(ballXPos, ballYPos, BALL_R, BALL_R);
		} else {
			//restarting round
			gc.setStroke(Color.YELLOW);
			gc.setTextAlign(TextAlignment.CENTER);
			gc.strokeText("Click to Start", width / 2, height / 2);
			ballXPos = width / 2;
			ballYPos = height / 2;
			ballXSpeed = new Random().nextInt(2) == 0 ? 1: -1;
			ballYSpeed = new Random().nextInt(2) == 0 ? 1: -1;
		}
		//Dealing with end of round
		if(ballYPos > height || ballYPos < 0) ballYSpeed *=-1;
		if(ballXPos < playerOneXPos - PLAYER_WIDTH) {
			scoreP2++;
			gameStarted = false;
		}
		if(ballXPos > playerTwoXPos + PLAYER_WIDTH) {  //meaning ball is out of sight
			scoreP1++;
			//if scoreP1 > maxScore then db.update()
			if (scoreP1 > maxScore) {
				TestMyDB user2 = db.where("NomP=?", nameRecall).first(TestMyDB.class);
	            user2.BestScore = scoreP1;
	            maxScore = scoreP1;
	            user2.NomG = "Pong";
	            db.update(user2); // adding score to DB
	            System.out.println("New best score !");
	            stage.setTitle("Connect&Play - " +nameRecall +" : Connected - Max score : " +maxScore);
			}
			//new round
			gameStarted = false;
		}
		//Accelerating ball
		if( ((ballXPos + BALL_R > playerTwoXPos) && ballYPos >= playerTwoYPos && ballYPos <= playerTwoYPos + PLAYER_HEIGHT) || 
			((ballXPos < playerOneXPos + PLAYER_WIDTH) && ballYPos >= playerOneYPos && ballYPos <= playerOneYPos + PLAYER_HEIGHT)) {
			ballYSpeed += 1 * Math.signum(ballYSpeed);
			ballXSpeed += 1 * Math.signum(ballXSpeed);
			ballXSpeed *= -1;
			ballYSpeed *= -1;
		}
		//Displayed text
		gc.fillText(scoreP1 + "\t\t\t\t\t\t\t\t" + scoreP2, width / 2, 100);
		gc.fillRect(playerTwoXPos, playerTwoYPos, PLAYER_WIDTH, PLAYER_HEIGHT);
		gc.fillRect(playerOneXPos, playerOneYPos, PLAYER_WIDTH, PLAYER_HEIGHT);
	}
    
	//HandleClick() on connect button
    class HandleClick implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent event) {
        	System.out.println(event.getEventType()); // affiche le type de l'event
        	System.out.println(event.getTarget()); // affiche la cible de l'event
        	//Button btn = (Button) event.getSource(); // affiche la source de l'event
        	
        	String nameRecon = text.getText();
        	nameRecall = nameRecon;
        	System.out.println(nameRecon);
        	String passRecon = pass.getText();
        	System.out.println(passRecon);
        	
        	String fakeRecon = "bla"; // for testing purpose
            
            //checking what line is selected. None for now
            //System.out.println(tempRecon.PassP);
            //db.insert(tempRecon); // gives error because NameP can't be null according to db restrictions
            
        	//Create a table object then use it to check if name and pass exist
            //String PassP = db.sql("select PassP from players where NomP like 'Bast'").first(String.class);
        	
        	//Creating a table related object from TestMyDB class
            TestMyDB user = db.where("NomP=? AND PassP=?", nameRecon, passRecon).first(TestMyDB.class);
            
            //Compare entered text and pass with DB. If so, player is connected.
            if (user != null) { // user with nameRecon AND passRecon exists
            	idPused = user.idP; // saving user id
            	maxScore = user.BestScore; // NB: BestScore can't be null (default 0)
            	System.out.println("Connected"); // just checking
            	lbl.setText("Connected");
            	lbl.setTextFill(Color.BLACK);
            	stage.setTitle("Connect&Play - " +nameRecon +" : Connected - Previous max score at Pong : " +maxScore);
            	//Supposed to block any further bad attempt?
            }
            else {
            	//Second connection with wrong credentials
            	lbl.setText("Not connected");
            	lbl.setTextFill(Color.INDIANRED);
            	stage.setTitle("Connect&Play");
            	idPused = 0;
            	maxScore = 0;
            	nameRecon = "";
            }
        }
    }
    
    //HandleClick() on play button
    class HandleClick2 implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent event) {
        	System.out.println(event.getEventType()); // affiche le type de l'event
        	System.out.println(event.getTarget()); // affiche la cible de l'event
        	//Button btn = (Button) event.getSource();
        	System.out.println(nameRecall);
        	System.out.println(idPused);
        	System.out.println(maxScore);
        	
        	if (lbl.getText() == "Connected") {
        		System.out.println("I am supposed to launch Pong and add a session");
        		
        		//Add a session entry in sessions table of database
        		//Creating a table related object from TestMySessions class
        		TestMySessions session = new TestMySessions();
                session.NomG = "Pong";
                session.idG = 1;
                session.idP = idPused; // using temp var idPused
                //setting DateTime of session in DB
                Timestamp ts = Timestamp.from(Instant.now());
                session.DateS = ts;
                //Adding the session
                if (idPused != 0) {
                	db.insert(session);
                	System.out.println("New session started");
                }
        		
        		//Pong canvas
                Canvas canvas = new Canvas(width, height);
        		GraphicsContext gc = canvas.getGraphicsContext2D();
        		Timeline tl = new Timeline(new KeyFrame(Duration.millis(10), e -> run(gc)));
        		tl.setCycleCount(Timeline.INDEFINITE);
        		canvas.setOnMouseMoved(e ->  playerOneYPos  = e.getY());
        		canvas.setOnMouseClicked(e ->  gameStarted = true);
        		stage.setScene(new Scene(new StackPane(canvas)));
        		stage.show();
        		tl.play();
        		System.out.println("Game successfully launched");
        	}
        }
    }
    
    //HandleClick() on Create button
    class HandleClick3 implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent event) {
        	System.out.println(event.getEventType()); // affiche le type de l'event
        	System.out.println(event.getTarget()); // affiche la cible de l'event
        	//Button btn = (Button) event.getSource();
        	System.out.println(nameRecall);
        	System.out.println(idPused);
        	System.out.println(maxScore);
        	
        	String nameCreate = text.getText();
        	System.out.println(nameCreate);
        	String passCreate = pass.getText();
        	System.out.println(passCreate);
        	
        	//Creating a table related object from TestMyDB class
        	TestMyDB user3 = db.where("NomP=?", nameCreate).first(TestMyDB.class);
        	//idPused = user3.idP; //temporary fix if idP is null here
        	
        	if (user3 == null) { // user doesn't already exist
        		if(nameCreate != "" && passCreate != "") {
        			TestMyDB user3b = new TestMyDB(); // user3 is null, we need a new object
        			user3b.NomP = nameCreate;
        			user3b.setPassP (passCreate); // using the setter of TestMyDB class because PassP is private
        			//user3b.NomG = "Pong"; // Needed IF NomG is NOT NULL
        			//NB: user3b.BestScore is an INT and set to 0 by JavaFX, so the DataBase NULL restriction won't give an error
        			db.insert(user3b);
        			System.out.println("New user created"); // just checking via sysout
        			lbl.setText("Please connect with new credentials.");
        			lbl.setTextFill(Color.PURPLE);
        			stage.setTitle("Connect&Play");
        		}
        		else {
        			lbl.setText("Please add a name and a password to do this.");
        			lbl.setTextFill(Color.INDIANRED);
        			stage.setTitle("Connect&Play");
                	idPused = 0;
                	maxScore = 0;
        		}
        	}
        	else if (user3 != null){
        		System.out.println("This name already exists");
        		lbl.setText("This name already exists.");
        		lbl.setTextFill(Color.INDIANRED);
        		stage.setTitle("Connect&Play");
            	idPused = 0;
            	maxScore = 0;
        	}
        }
    }

    public static void main(String[] args) {
        launch();
    }
}
