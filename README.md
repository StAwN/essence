# Essential Arcade Games

## Requirements

Please download and install **Eclipse IDE for Java developers (including Maven)** to run this app: https://www.eclipse.org/downloads/packages/release/2021-06/r/eclipse-ide-java-developers
**Create the required database using the provided SQL file**, with your favorite MySQL client (phpMyAdmin, TablePlus, WorkBench, etc.)  
When running the DB locally, you will need a software like **Wamp or Xamp** (Wamp available here: https://www.wampserver.com/en/download-wampserver-64bits/).

## How to run the app

- In Eclipse, click **File**, choose **Import Project**, **Maven**, **Existing Maven Projects**, **Browse**, and double click the repository you just downloaded.  
- Validate by clicking **Select Folder** then **Finish**. You may need to add a template name in **Advanced** if you already imported a repository with the same name.  
- Now select the project in Eclipse on the left and double click on any file, then click on the **Run App** button (green arrow).

Note: It uses **NORM** SQL implementation (Copyright 2014, Dieselpoint, Inc. under Apache License v2.0). This will display a harmless console warning: https://github.com/dieselpoint/norm.
