#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------

CREATE DATABASE IF NOT EXISTS pong_csc_12;

#------------------------------------------------------------
# Table: players
#------------------------------------------------------------

CREATE TABLE players(
        idP       Int  Auto_increment  NOT NULL ,
        NomP      Varchar (100) NOT NULL ,
        BestScore Int NOT NULL DEFAULT 0 ,
        NomG      Varchar (100) DEFAULT "Pong" ,
        PassP     Varchar (100) 
	,CONSTRAINT PLAYERS_PK PRIMARY KEY (idP)
)ENGINE=MyISAM;


#------------------------------------------------------------
# Table: sessions
#------------------------------------------------------------

CREATE TABLE sessions(
        idS    Int  Auto_increment  NOT NULL ,
        idP    Int NOT NULL ,
        idG    Int NOT NULL ,
        NomG   Varchar (50) NOT NULL ,
        DateS  Datetime COMMENT "session date" 
	,CONSTRAINT sessions_PK PRIMARY KEY (idS)
)ENGINE=MyISAM;


#------------------------------------------------------------
# Table: games
#------------------------------------------------------------

CREATE TABLE games(
        idG        Int  Auto_increment  NOT NULL ,
        NomG       Varchar (50) NOT NULL ,
        TypeG      Varchar (50) 
	,CONSTRAINT games_PK PRIMARY KEY (idG)
)ENGINE=MyISAM;

INSERT INTO `games`(`idG`, `NomG`, `TypeG`) VALUES ('1', 'Pong', 'Classic');


#------------------------------------------------------------
# Table: play
#------------------------------------------------------------

CREATE TABLE play(
        idP Int NOT NULL ,
        idS Int NOT NULL ,
        idG Int NOT NULL
	,CONSTRAINT PLAY_PK PRIMARY KEY (idP,idS,idG)

	,CONSTRAINT PLAY_PLAYERS_FK FOREIGN KEY (idP) REFERENCES PLAYERS(idP)
	,CONSTRAINT PLAY_SESSIONS0_FK FOREIGN KEY (idS) REFERENCES SESSIONS(idS)
	,CONSTRAINT PLAY_GAMES1_FK FOREIGN KEY (idG) REFERENCES GAMES(idG)
)ENGINE=MyISAM;

